<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'val');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S><3N tX:oypu8XwUuVK`u`A!rZ`wSf8SdBO/!$,g:sJ~DQU0&KFRu(qUDnhL!s.');
define('SECURE_AUTH_KEY',  'yi3;BCYUh;k|To)}agx|27&H~HhVECt/xkbkM#F+i$c&h=Uk*XUEWdR]Fb}Tb DS');
define('LOGGED_IN_KEY',    ']G;[w[Ovo`b*e>-(wT^a<Dzv%FH_~`{A av#Bt~b>e>uLcA3E*!{t&`4S}cEt4Xk');
define('NONCE_KEY',        'XR*Q|_,99]FZgvKS}zF$[11X%XF}MRN^Ms?JN7e>dXbY[#bU2IP:e`:}VG0tkG|K');
define('AUTH_SALT',        '5aF*T3V$OK]Oh-9#2rBvY_*c64mRQM5=*5vUlglAYM[/c$OWDG|u{%bdx%OiG9ab');
define('SECURE_AUTH_SALT', 'AVdr-Fc1 d(^sqk~S{c4MY.6iT-n!JdKx<0qqi?Y*h&%):El|Q5<6)%x#b1-{m;K');
define('LOGGED_IN_SALT',   'bGdxc4|E::BTkEGy3mb^3IS3m6P y4R:/Y{)gy@A:~eq`PV:*MY(H,FSmT@/59{k');
define('NONCE_SALT',       'HQc6=b]p5<n,^gPFQR`k7|e]En5*vtM&I6C^EN0Pr<&Dc6zg}rC2r{oW5O6)VS},');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'val_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
