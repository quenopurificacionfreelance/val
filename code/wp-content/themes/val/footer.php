<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Val
 */
?>


<?php wp_footer(); ?>

<section class="footer">
	<div class="container">
		
	</div>
	<div class="footer-bar">
		<div class="container">
			<div class="grid_6 omega"></div>
			<div class="grid_6 omega delta"><span class="right"><?php the_field('copyright', 'options'); ?></span></div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
</body>
</html>