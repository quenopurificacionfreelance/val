<?php
/**
 * Peachtree functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Val
 */

add_filter('show_admin_bar', '__return_false'); //remove admin bar


if ( ! function_exists( 'val_setup' ) ) :

    function val_setup() {
        
        register_nav_menus( array(
            'menu-1' => esc_html__( 'Primary', 'val' ),
            'menu-2' => esc_html__( 'Footer', 'val' ),
        ) );

    }
endif;
add_action( 'after_setup_theme', 'val_setup' );

add_theme_support( 'post-thumbnails' ); 

function val_scripts() {
    wp_enqueue_style( 'val-grid', get_template_directory_uri() . '/assets/css/grid.css' );
    wp_enqueue_style( 'val-flexslider', get_template_directory_uri() . '/assets/css/flexslider.css' );
    wp_enqueue_style( 'val-style', get_stylesheet_uri() );
    wp_enqueue_style( 'val-main', get_template_directory_uri() . '/assets/css/main.css' );

    wp_enqueue_script( 'val-flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider-min.js', array('jquery'), '20181007', true );

    wp_enqueue_script( 'val-script', get_template_directory_uri() . '/assets/js/script.js', array(), '20181007', true );
    if(is_page(58)) { 
        wp_enqueue_script( 'val-masonry', get_template_directory_uri() . '/assets/js/masonry.pkgd.min.js', array(), '20181007', true );
        wp_enqueue_script( 'val-multipleFilterMasonry', get_template_directory_uri() . '/assets/js/multipleFilterMasonry.js', array(), '20181007', true );
    }

    if(is_page(13)) {        
        wp_enqueue_script( 'val-map', get_template_directory_uri() . '/assets/js/map.js', array(), '20181007', true );
    }
    
}
add_action( 'wp_enqueue_scripts', 'val_scripts' );

function val_widgets_init() {

    register_sidebar( array(
        'name' => 'Footer Newsletter',
        'id' => 'footer_subscription',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'val' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'val' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'val_widgets_init' );

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'thumbnails' );    
    add_image_size( 'featured-thumb', 486, 283 ); 
    add_image_size( 'featured-news', 680, 282 ); 
    add_image_size( 'featured-loc', 234, 234 ); 
}

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
    return '<p><a class="moretag" href="'. get_permalink($post->ID) . '"> Read more</a></p>';
}
add_filter('excerpt_more', 'new_excerpt_more');


function my_acf_google_map_api( $api ){    
    $api['key'] = 'AIzaSyBFVbolIeDixdWT_EYJakucozB5oowHrN8';
    return $api;    
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field');
function add_default_value_to_image_field($field) {
    acf_render_field_setting( $field, array(
        'label'			=> 'Default Image',
        'instructions'		=> 'Appears when creating a new post',
        'type'			=> 'image',
        'name'			=> 'default_value',
    ));
}

// if( function_exists('acf_add_options_page') ) {    
//     acf_add_options_page();    
// }

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Theme Header Settings',
    //     'menu_title'    => 'Header',
    //     'parent_slug'   => 'theme-general-settings',
    // ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Theme Footer Settings',
    //     'menu_title'    => 'Footer',
    //     'parent_slug'   => 'theme-general-settings',
    // ));
    
}

// require get_template_directory() . '/inc/customizer.php';

// require 'inc/facts.php';
require 'inc/students.php';
// require 'inc/team.php';
// require 'inc/locations.php';
// require 'inc/jobs.php';

