<script type="text/javascript">
    ( function( $ ) {
        $(window).load(function() {

          $('.slider-counter-'+<?php echo $counter; ?>).flexslider({
            animation: "slide"
          });
        });
    } )( jQuery );        
</script>

<section class="carousel_banner slider section-<?php echo $counter; ?>">    
    <div class="flexslider slider-counter-<?php echo $counter; ?>">
        <ul class="slides">
        <?php while( have_rows('carousel') ): the_row(); 

            $banner_image = get_sub_field('banner_image');
            $bgcolor = get_sub_field('background_color');
            $banner_content = get_sub_field('banner_content');
            $content_alignment = get_sub_field('content_alignment');
            
            ?>

            <li class="slide" style="background-color: <?php echo $bgcolor; ?>;">      
                <img src="<?php echo $banner_image['url']; ?>" alt="<?php echo $banner_image['alt']; ?>">
                <div class="container">
                    <div class="caption <?php echo $content_alignment; ?>" >
                        <p class="flex-caption">
                            <?php echo $banner_content; ?>                    
                        </p>
                    </div>
                </div>
            </li>

        <?php endwhile; ?>
        </ul>
    </div>
    <div class="clearfix"></div>
</section>