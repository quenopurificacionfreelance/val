<?php
    $container_class = get_sub_field('container_class');            
    $background_color = get_sub_field('background_color');            
    $background_content_fullwidth = get_sub_field('background_content_fullwidth');            
    $content = get_sub_field('content');
    if($background_content_fullwidth != null) {
        $textColor = 'white';
    } else {
        $textColor = 'default';
    }
    ?>
    <section class="content section-<?php echo $counter.' '.$textColor; ?>" style="background-image: url(<?php echo $background_content_fullwidth['url']; ?>); background-color: <?php echo $background_color; ?>;">   
        <div class="container"> 
            <div class="<?php echo $container_class; ?> omega">
                <?php echo $content; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
            