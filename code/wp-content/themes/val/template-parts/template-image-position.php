<section class="image-position">
	<div class="spacer">
		<div class="container">
	        <div class="grid_12 omega">
	        	<div class="content-title">
	        		<h2 class="aligncenter"><?php echo $label; ?></h2>
	        	</div>
	        	
	        	<?php 
	        		while( have_rows('content_repeater') ): the_row(); 
	                    $thumbnail_image = get_sub_field('thumbnail_image');                    
	                    $thumbnail_position = get_sub_field('thumbnail_position');                    
	                    $content = get_sub_field('content');                    
	                    ?>
	                    <img src="<?php echo $thumbnail_image['url']; ?>" alt="<?php echo $thumbnail_image['alt']; ?>" class="img-responsive <?php echo $thumbnail_position; ?>">
			        	<div class="content">
			        		<?php echo $content; ?>
			        	</div> <?php
	                endwhile;
	        	?>
	        </div>
	        <div class="clearfix"></div>
	    </div>
	</div>
</section>


