<?php
/**
 * Spectrum Theme Customizer
 *
 * @package Spectrum
 */

function spectrum_customize_register( $wp_customize ) {

    //social media
    $wp_customize->add_section( 'social_links', array(
        'title' => 'Social Media Links',
    ) );

    $wp_customize->add_setting( 'sml_facebook', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_facebook', array(
        'label' => 'Facebook',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    $wp_customize->add_setting( 'sml_twitter', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_twitter', array(
        'label' => 'Twitter',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    $wp_customize->add_setting( 'sml_linkedin', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_linkedin', array(
        'label' => 'LinkedIn',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    $wp_customize->add_setting( 'sml_instagram', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_instagram', array(
        'label' => 'Instagram',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    $wp_customize->add_setting( 'sml_youtube', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_youtube', array(
        'label' => 'YouTube',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    $wp_customize->add_setting( 'sml_googleplus', array(
        'sanitize_callback' => 'esc_url_raw'
    ) );

    $wp_customize->add_control( 'sml_googleplus', array(
        'label' => 'Google+',
        'section' => 'social_links',
        'type' => 'url',
    ) );

    //site info
    $wp_customize->add_section( 'site_info', array(
        'title' => 'Site Info',
        'priority'   => 20,
    ) );

    $wp_customize->add_setting( 'contact_info_phone', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    ) );

    $wp_customize->add_control( 'contact_info_phone', array(
        'label' => 'Phone',
        'section' => 'site_info',
        'type' => 'text',
    ) );

    $wp_customize->add_setting( 'contact_info_email', array(
        'sanitize_callback' => 'sanitize_email'
    ) );

    $wp_customize->add_control( 'contact_info_email', array(
        'label' => 'Email',
        'section' => 'site_info',
        'type' => 'email',
    ) );

    $wp_customize->add_setting( 'copyright', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    ) );

    $wp_customize->add_control( 'copyright', array(
        'label' => 'Copyright',
        'section' => 'site_info',
        'type' => 'text',
    ) );

}
add_action( 'customize_register', 'spectrum_customize_register' );

?>