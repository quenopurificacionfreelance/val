<?php

add_action('init', 'team_register');

//Custom Post Type for Homepage Thumbnails
function team_register() {
	$labels = array(
		'name' => _x('Team', 'post type general name'),
		'singular_name' => _x('Team', 'post type singular name'),
		'add_new' => _x('Add new member', 'member item'),
		'add_new_item' => __('Add member'),
		'edit_item' => __('Edit member'),
		'new_item' => __('New member'),
		'view_item' => __('View member'),
		'search_items' => __('Search member'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
        'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('team', $args );
}

//create a function that will attach our new 'member' taxonomy to the 'post' post type
function add_location_taxonomy_to_post(){

    //set the name of the taxonomy
    $taxonomy = 'location';
    //set the post types for the taxonomy
    $object_type = 'team';
    
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'Locations',
        'singular_name'      => 'Location',
        'search_items'       => 'Search Locations',
        'all_items'          => 'All Locations',
        'parent_item'        => 'Parent Location',
        'parent_item_colon'  => 'Parent Location:',
        'update_item'        => 'Update Location',
        'edit_item'          => 'Edit Location',
        'add_new_item'       => 'Add New Location', 
        'new_item_name'      => 'New Location Name',
        'menu_name'          => 'Location'
    );
    
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'location')
    );
    
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init','add_location_taxonomy_to_post');