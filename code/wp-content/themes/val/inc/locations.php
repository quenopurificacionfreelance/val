<?php

add_action('init', 'locations_register');

//Custom Post Type for Homepage Thumbnails
function locations_register() {
	$labels = array(
		'name' => _x('Locations', 'post type general name'),
		'singular_name' => _x('Location', 'post type singular name'),
		'add_new' => _x('Add new location', 'Location item'),
		'add_new_item' => __('Add location'),
		'edit_item' => __('Edit location'),
		'new_item' => __('New location'),
		'view_item' => __('View location'),
		'search_items' => __('Search location'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('locations', $args );
}