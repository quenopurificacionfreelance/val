<?php

add_action('init', 'students_module');

//Custom Post Type for Homepage Thumbnails
function students_module() {
	$labels = array(
		'name' => _x('Students Module', 'post type general name'),
		'singular_name' => _x('Student', 'post type singular name'),
		'add_new' => _x('Add new student', 'Student item'),
		'add_new_item' => __('Add student'),
		'edit_item' => __('Edit student'),
		'new_item' => __('New student'),
		'view_item' => __('View student'),
		'search_items' => __('Search student'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('students', $args );
}