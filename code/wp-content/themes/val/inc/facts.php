<?php

add_action('init', 'facts_register');

//Custom Post Type for Homepage Thumbnails
function facts_register() {
	$labels = array(
		'name' => _x('Facts', 'post type general name'),
		'singular_name' => _x('Facts', 'post type singular name'),
		'add_new' => _x('Add new Facts', 'Facts item'),
		'add_new_item' => __('Add Facts'),
		'edit_item' => __('Edit Facts'),
		'new_item' => __('New Facts'),
		'view_item' => __('View Facts'),
		'search_items' => __('Search Facts'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('facts', $args );
}