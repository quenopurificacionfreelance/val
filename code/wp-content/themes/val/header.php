<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Val
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFVbolIeDixdWT_EYJakucozB5oowHrN8"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
	if(is_front_page()) {
		$enable = get_field('enable', 'option');
		if($enable == 'enable') {
		    include( locate_template( 'template-parts/option-template-scroll.php', false, false ) );
		}
	}		
?>
<header>
    <div class="header-container">
    	<div class="grid_5 omega">
    		<div class="logo">
    			<?php
                    $logo = get_field('logo', 'option');
                ?>
                <a href="<?php echo get_home_url(); ?>">
                	<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="img-responsive">
                </a>
    		</div>
    	</div>
    	<div class="grid_7 omega">
    		<div class="navigation">
    			<div class="grid_12 omega">
    				<div class="search-box">
    					
    				</div>
    				<div class="monetary">

    				</div>
    				<div class="clearfix"></div>
    			</div>
    			<div class="grid_12 omega">
    				<nav>
    					<?php wp_nav_menu( array(
		                        'theme_location' => 'menu-1',
		                        'menu_class' => 'clearfix'
		                    ) );
		                ?>
    				</nav>
    				<div class="clearfix"></div>
    			</div>
    		</div>
    	</div>
    	<div class="clearfix"></div>
    </div>
</header>
<div class="header-spacer"></div>