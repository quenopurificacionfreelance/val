<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Val
 */

get_header(); ?>

<header class="page-header bg-green">
    <div class="container clearfix">
        <div class="grid_12 omega">
        <?php if ( have_posts() ) : ?>
                <?php
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                    the_archive_description( '<div class="taxonomy-description">', '</div>' );
                ?>
        <?php endif; ?>
        </div>
    </div>
</header>

 <div class="container clearfix">

    <div class="grid_8">
        <main id="main" class="site-main">

        <?php
        if ( have_posts() ) :

            /* Start the Loop */
            while ( have_posts() ) : the_post();

                /*
                    * Include the Post-Format-specific template for the content.
                    * If you want to override this in a child theme, then include a file
                    * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                    */
                get_template_part( 'template-parts/content', get_post_format() );

            endwhile;

            the_posts_pagination( array(
                'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                'before_page_number' => '<span class="screen-reader-text">' . __( 'Page ', 'pcre' ) . '</span>',
            ));

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif; ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <?php get_sidebar(); ?>

</div>

<?php
get_footer();
?>