( function( $ ) {

    $(window).load(function() {
      $('.flexslider').flexslider({
        animation: "slide"
      });
    });

    // $(function(){      
    //   // Now bind the event to the desired element
    //   $('section.scroll_banner').on('mousewheel', function(e){
    //     e.preventDefault();        
    //     $('html, body').animate({ scrollTop: $('#main').offset().top}, 1000, 'linear');
    //   });
    // });

    $(function() {
      $('section.scroll_banner a[href*=#main]').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 1000, 'linear');
      });
    });
    console.log($('.scroll_banner').length);
    $(document).ready(function(){
      $(window).resize(function(){
        var windowHeight = $(window).height();
        var ninetypercent = 1 * windowHeight;
        $(document).scroll(function(){        

          if($('.scroll_banner').length) {
            var y = $(this).scrollTop();    
            if( y > ninetypercent) {
              $('header').addClass('shrink');
              $('.header-spacer').addClass('active');
            } else {
              // Else remove it.
              $('header').removeClass('shrink');
              $('.header-spacer').removeClass('active');
            }
          } else {
            $('header').addClass('shrink');
            $('.header-spacer').addClass('active');
          }
          
        });      
      }).resize();      
    }); 


} )( jQuery );

